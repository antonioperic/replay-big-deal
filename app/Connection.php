<?php

class Connection
{
    private static $instance;

    private $conn;

    /**
     * @return Connection
     */
    public static function init()
    {
        self::$instance = (self::$instance instanceof self) ? self::$instance : new Connection();

        return self::$instance;
    }
    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->conn;
    }

    private function __construct()
    {
        $this->conn = new PDO('mysql:host=localhost;dbname=lokastik_replay', 'lokastik_replay', 'replay_bmw320i', array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ));
    }
}